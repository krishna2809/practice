
class Stack:
    def __init__(self, stackSize, arr):
        self.arr = arr
        self.size = stackSize -1 
        self.top = self.size 

    def push(self, element):
	    # if(<= self.size)
        if(self.top < self.size):
            self.top += 1 
            self.arr[self.top] = element
            

        else:
	        print("******** Stack is Overflow ******** \n")	
    def pop(self):
        if(self.top > -1 ):
            # removeElement = self.arr.pop()
            removeElement = self.arr[self.top]
            print(f"{removeElement} has been removed :\n")
            self.top -= 1 
        else:
	        print("******** Stack is underflow ********\nPlease First Enter a element \n")



    def peep(self):
	     if(self.top > -1):
	        print(f"Top element of Stack : {self.arr[self.top]} ") 
	     else:
	        print("******** Stack is Empty ******** \n")   

    def showStack(self):
        if(self.top > -1):
            for i in range(self.top+1):
                print(self.arr[i], end=" ")  
        else:
              print("******** Stack is empty ********  ")
        print("\n")


stackSize = int(input("Enter size of stack : "))
# arr = [ int(input("Enter element of stack \n")) for i in range(stackSize)]
arr = list(map(int, input("Enter element With space : ").split(" ")))
stack = Stack(stackSize, arr)
# choice   = int(input("Enter you choice :\n[1] Push \n[2] pop \n[3] peep \n[4] showStack \n[5] Exit \n" ))

while True:
    choice   = int(input("\nEnter you choice NUmber:\n[1] Push \n[2] pop \n[3] peep \n[4] showStack \n[5] Exit \n" ))

    if(choice == 1):
	    stack.push(input("\nEnter a element : "))

    if(choice == 2):
	    stack.pop()
    if(choice == 3):
	    stack.peep()   
    if(choice == 4):
	    stack.showStack()
    if(choice == 5):
	    del stack
	    print("Thank you for doing some operations \n")
	    break         


