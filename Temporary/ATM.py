# administator ----> UserName : krishna, password: krishna123

import random
limit = 0

#------------------------------ USER Details of Account ------------------------------------------ 

user_details = {
    "BOB" : {
        1000 : {"username" : "krishna", "userpin":10,"balance": 50000,"mobile number":9999999999,"Account Number":1789654},
        2000 : {"username" : "vishal", "userpin":20,"balance": 50000,"mobile number":9999999991,"Account Number":1789657},
    },
    "SBI" : {
        3000 : {"username" : "krishna", "userpin":30,"balance": 50000,"mobile number":9999999992,"Account Number":1789647},
        4000 : {"username" : "vishal", "userpin":44,"balance": 30000,"mobile number":9999999994,"Account Number":1789654}
    }
}

#------------------------------ ATM Details -----------------------------------------------------

atm_dict = {
    "BOB" : {
        "bob_atm_modasa" : {"total_atm_amount" : 100000,"Location" : "modasa"},
        "bob_atm_ahmedabad" : {"total_atm_amount" : 100000,"Location" : "Ahmedabad"}
    },
    "SBI" : {
        "sbi_atm_modasa" : {"total_atm_amount" : 100000,"Location" : "modasa"},
        "sbi_atm_ahmedabad" : {"total_atm_amount" : 100000,"Location" : "Ahmedabad"}
    }
}
# ----------------------- Administrator Details ---------------------------------------
administrator_details = {
    'krishna': {'password': 'krishna123', 'email_id': 'krishna_singh@gamil.com', 'Phone Number': 9999999999}
}

#----------------- User validation --------------------------------------------
def userValidation(bankname,atmname):
    return getDetails(bankname)


# --------------- Get details from user side ------------------------------------
def getDetails(bankname):
    is_invalid = True
    user_detail = user_details[bankname]
    card_number = int(input("\nEnter Card Number : "))
    pin_number =  int(input("\nEnter PIN Number  : "))
    data = user_detail.get(card_number,"Invalid Card Number")
    if card_number in user_detail:
        if pin_number in data.values():
            displayOption(data)
            is_invalid = False
        else: 
            print("\n {0:*^50s}  \n ".format(" Invalid Pin Number "))
            return is_invalid
    else:
        print("\n {0:*^50s}  \n ".format(" Invalid Card Number "))
        return is_invalid          
      
#----------------- Display User Details ---------------------------------------------------
def displayDetails(detail):
    print("\n {0:*^60s}\n".format(" YOUR DETAILS "))

    print(f"Bank Name : {detail['username']}  \t\tAccount Number : {detail['Account Number']}\n")
    print(f"Balance   : {detail['balance']}    \t\tMobile Number : {detail['mobile number']}\n")
    print("\n {0:*^60s}".format(""))

# ------------------- Normal User Service  ------------------------------
def displayOption(data):
    print("\n[1] Withdraw \t[2] Deposite \t[3] Display \t[4] Quit\n")
    choice = int(input("\nChoose option : "))
    if(choice == 1):
        withdraw(data)
        displayOption(data)
    elif(choice == 2):
        deposite(data)
        displayOption(data)
    elif(choice == 3):
        displayDetails(data)
        displayOption(data)
    elif(choice == 4):
        print("\n {0:*^50s}  \n ".format(" Thank You  "))

    else:
        print("\nWrong choice \n")

# ------------------- Withdraw  ------------------------------
def withdraw(data):
    global limit,atm_dict, total_withdraw,  day_limit_transaction, one_time_transaction
    withdraw_amount = int(input("\nEnter Withdraw Amount : "))
    atm = atm_dict[bank_name][atm_name]
    user_amount = data['balance']
    atm_amount = atm['total_atm_amount']
    if(total_withdraw < day_limit_transaction ):
        if(withdraw_amount <= one_time_transaction):
            if(limit < 5):
                if(withdraw_amount%100 == 0 ):
                    if(withdraw_amount <= atm_amount):
                        if(withdraw_amount <= user_amount):
                            if bank_name == "BOB":
                                total_amount = user_amount - withdraw_amount
                                atm_amount = atm_amount - withdraw_amount
                                data['balance'] = total_amount
                                atm['total_atm_amount'] = atm_amount

                                total_withdraw += withdraw_amount
                                #print(data['balance'])
                                # print(atm['total_atm_amount'])
                                limit += 1 
                                print("\n {0:*^50s}  \n ".format(" Successfully Withdraw Amount "))
                                print("\n {0:*^50s}  \n ".format(' Your Available Amount : ' + str(data['balance'])+' '))

                            else:
                                updated_withdraw_amount = withdraw_amount*0.05
                                after_cut_five_per =  user_amount -  updated_withdraw_amount
                                if(withdraw_amount <= after_cut_five_per):
                                    total_amount = user_amount - withdraw_amount - updated_withdraw_amount
                                    atm_amount = atm_amount - withdraw_amount - updated_withdraw_amount
                                    data['balance'] = total_amount
                                    atm['total_atm_amount'] = atm_amount
                                    total_withdraw += withdraw_amount
                                   # print(data['balance'])
                                    # print(atm['total_atm_amount'])
                                    limit += 1
                                    print("\n {0:*^50s}  \n ".format(" Successfully Withdraw Amount "))
                                    print("\n {0:*^50s}  \n ".format(' Your Available Amount : ' + str(data['balance'])+' '))
                                else:
                                    print("\n {0:*^100s}  \n ".format(" You can't withdraw Amount because " + str(after_cut_five_per) + ' After cut 5 % Amount '))      

                        else:
                            print(f"\nEnter withdraw amount less than or equal to {user_amount}")
                    else:
                        print(f"\nAtm Available Amount :  {atm_amount}")
                else:
                    print("\n {0:*^50s} \n".format(" Multiple of 100  "))       
            else:
                print(f"\nYour one day limit is over.\n")
        else:
            print("\n {0:*^70s}  \n ".format(" One time 10,000 Amount withdraw from ATM "))
             


    else:
        print("\n {0:*^70s}  \n ".format(" One Day only 30,000 rupees withdrawal from ATM "))
              
# ---------------------- Deposite --------------------------------------------------------
def deposite(data):
    global atm_dict, atm_name, bank_name
    
    deposite_amount = int(input("\nEnter Deposite Amount : "))
    if(deposite_amount%100 == 0 ):
        atm = atm_dict[bank_name][atm_name]
        user_amount = data['balance']
        atm_amount = atm['total_atm_amount']
        update_amount = user_amount + deposite_amount
        data['balance'] = update_amount
        atm_amount = atm_amount + deposite_amount
        atm['total_atm_amount'] = atm_amount
        # print(data['balance'])
        # print(atm['total_atm_amount']) 
        print("\n {0:*^50s}  \n ".format(" Successfully Deposit Amount "))
        print("\n {0:*^50s}  \n ".format(' Your Available Amount : ' + str(data['balance'])+' '))
    else:
        print("\n {0:*^50s} \n".format(" Multiple of 100  "))     

# ---------------------------- Administrator Feature ------------------

def moreFeature():
    is_admin_continue = True
    print("\n[1] User \t[2] ATM \t[3] Banks \t[4] Add Money in ATM \t[5] Quit")
    choice = int(input("\nChoose option : "))
    if(choice == 1):
        print("\n[1] Insert User \t[2] Delete User \t[3] Update User \t[4] Show User Details \t[5] Quit")
        user_choice = int(input("\nChoose option : "))
        if(user_choice == 1):
            insertUser()
        elif(user_choice == 2):
            deleteUser()
        elif(user_choice == 3):
            updateUser()
        elif(user_choice == 4):
             show_users_details()
        elif(user_choice == 5):
            print("\n {0:*^50s}  \n ".format(" Thank You  "))        
        else:
            print('\nWrong choice\n')
    elif(choice == 2):
        # deposite(data)
        # displayOption(data)
        print("\n[1] Insert ATM \t[2] Delete ATM \t[3] Update ATM \t[4] Show User ATM Details \t[5] Quit")
        user_choice = int(input("\nChoose option : "))
        if(user_choice == 1):
            insertAtm()
        elif(user_choice == 2):
            deleteAtm()
        elif(user_choice == 3):
            updateAtm()
        elif(user_choice == 4):
           show_atm_details()
        elif(user_choice == 5):
            print("\n {0:*^50s}  \n ".format(" Thank You  "))              
        else:
            print('\nWrong choice\n')
        
    elif(choice == 3):
        print("\n[1] Insert Bank \t[2] Delete Bank \t[3] Update Bank \t[4] Show User Bank Details \t[5] Quit")
        user_choice = int(input("\nChoose option : "))
        if(user_choice == 1):
            insertBank()
        elif(user_choice == 2):
            deleteBank()
        elif(user_choice == 3):
            updateBank()
        elif(user_choice == 4):
            show_users_details()

        elif(user_choice == 5):
           print("\n {0:*^50s}  \n ".format(" Thank You  "))   
           is_admin_continue = False
           return is_admin_continue  
        else:
            print('\nWrong choice\n')
    elif(choice == 4):
        insert_money_atm()
    elif(choice == 5):
        print("\n {0:*^50s}  \n ".format(" Thank You  "))
        is_admin_continue = False
        return is_admin_continue
             
    else:
        print("\nWrong choice\n")
    return is_admin_continue    

# ------------------- Insert Users by Administrator   ------------------------------
def insertUser():
    bank_name = bank_list()
    is_invalid = True
    if(bank_name != is_invalid):
        # print(userDetails)
        user_dict = {}
        user_name = input("\nEnter User Name : ")
        user_pin = random.randint(10,99)
        user_card_number = random.randint(1000,9999)
        # balance = int(input("\nEnter balance : "))
        mobile_number = int(input("\nEnter Mobile Number : "))
        Account_number = int(input("\nEnter Account Number : "))
        user_dict = {"username":user_name,"userpin":user_pin,"balance": 0,"bankname":bank_name,"mobile number":mobile_number,"Account Number":Account_number}
        user_details[bank_name][user_card_number] = user_dict
        print("\n {0:*^50s}  \n ".format(" Successfully User Details Inserted "))


        # print('updated details 1', user_details)
        # print('updated details 2', userDetails)
        # print('updated user_details[bank_name][user_card_number]', user_details[bank_name][user_card_number])



# -------------------  Users Deletes by Administrator   ------------------------------


def deleteUser():
    is_invalid  = True
    bank_name = bank_list()
    if(bank_name != is_invalid): 

        # global user_details,bank_name
        # print("User Deatisl---->  ",user_details[bank_name])
        card_number = int(input("\nEnter card Number : "))
        if(card_number in user_details[bank_name]):

            del user_details[bank_name][card_number]
            print("\n {0:*^50s}  \n ".format(" Successfully User Details Deleted "))
        else:  
            print("\n {0:*^50s}  \n ".format(" Invalid Card Number "))

        print(user_details)

# -------------------  Users Update by Administrator   ------------------------------

    
def updateUser():
    is_invalid = True
    bank_name = bank_list()
    if(bank_name != is_invalid):
    # global user_details,bank_name
        user_dict = {}
        user_card_number = int(input("\nEnter Card Number : "))
        #print(userDetails[bank_name][user_card_number])
        if user_card_number in user_details[bank_name]:
            user_name = input("\nEnter User Name : ")
            # user_pin = input("\nEnter pin number ")
            mobile_number = int(input("\nEnter Mobile Number : "))
            user_details[bank_name][user_card_number]['username'] = user_name
            # user_details[bank_name][user_card_number]['userpin'] = user_pin
            user_details[bank_name][user_card_number]['mobile number'] = mobile_number
            print("\n {0:*^50s}  \n ".format(" Successfully Update User Details "))

            print(user_details)
        else:
            print("\n {0:*^50s}  \n ".format(" Invalid Card Number "))

# -------------------  NEW ATM LSIT ADD  by Administrator   ------------------------------


def insertAtm():
    is_invalid = True
    bank_name = bank_list()

    if(bank_name != is_invalid):
        which_atm = input("\nEnter ATM Name : ")
        location = input("\nEnter Location of ATM : ")
        atm_amount = int(input("\nEnter ATM Amount : "))
        # {"total_atm_amount" : 40000,"Location" : "rajkot"},
        atm_details = {"total_atm_amount": atm_amount, 'Location': location}
        atm_dict[bank_name][which_atm] = atm_details
        print("\n {0:*^50s}  \n ".format(" Successfully Insert ATM Details "))
        # print("atm_dict ---> ", atm_dict)

# -------------------  Delete exist ATM by Administrator   ------------------------------

def deleteAtm():
    is_invalid = True
    bank_name = bank_list()
    if(bank_name != is_invalid):
        atm_name =  atm_list(bank_name)
        if(atm_name != is_invalid):
           del atm_dict[bank_name][atm_name]
           # print(atm_dict)
           print("\n {0:*^50s}  \n ".format(" Successfully Deleted ATM Details "))   

# -------------------  Update exist ATM by Administrator   ------------------------------

        
def updateAtm():
    is_invalid = True
    bank_name = bank_list()
    if(bank_name != is_invalid):
        atm_name = atm_list(bank_name)
        if(atm_name != is_invalid):
        # if(bank_name != is_invalid and atm_name != is_invalid):
            atm_details = atm_dict[bank_name][atm_name]
            atm_details['Location'] = input("\nEnter ATM location : ")
            atm_details['total_atm_amount'] = int(input("\nEnter Amount : "))
            # print(atm_dict[bank_name])

            print("\n {0:*^50s}  \n ".format(" Successfully Updated  ATM Details "))

# -------------------  Insert Money exist ATM by Administrator   ------------------------------


def insert_money_atm():
    is_invalid = True
    bank_name = bank_list()
    if(bank_name != is_invalid):
        atm_name = atm_list(bank_name)
        if(atm_name != is_invalid):
    # if(bank_name != is_invalid and atm_name != is_invalid):
            atm_dict[bank_name][atm_name]['total_atm_amount'] += int(input("\nEnter Amount for insert Money into ATM :  "))
            # print('Updated amount : ',atm_dict[bank_name][atm_name])
            print("\n {0:*^50s}  \n ".format(" Successfully Insert Money in  ATM "))

# -------------------  New Bank Insert in Bank dict and ATM dict by Administrator   ------------------------------

def insertBank():
    new_Bank_name = input("\nEnter new Bank Name : ")
    user_details[new_Bank_name] = {}
    atm_dict[new_Bank_name] = {}
    print("\n {0:*^50s}  \n ".format(" Successfully Insert Bank Details "))
    # print(user_details)

# -------------------  Delete Bank in BANK Dict and Atm Dict   by Administrator   ------------------------------


def deleteBank():
    is_invalid = True
    bank_name = bank_list()
    if(bank_name != is_invalid):
       del user_details[bank_name]
       if(bank_name in atm_dict):
          del atm_dict[bank_name]
          # print('user_details : ', user_details)
          # print('atm_dict : ', atm_dict)
          print("\n {0:*^50s}  \n ".format(" Successfully Delete Bank Details "))


# -------------------  Update Bank in BANK Dict and Atm Dict   by Administrator   ------------------------------


def updateBank():
    is_invalid = True
    bank_name = bank_list()
    if(bank_name != is_invalid):
       bank_details =  user_details[bank_name]
       atm_details =  atm_dict[bank_name]
       update_bank_name = input("Enter Update Bank Name : ")
       user_details[update_bank_name] = bank_details 
       atm_dict[update_bank_name] = atm_details
       del user_details[bank_name]
       del atm_dict[bank_name]
       # print('user_details : ', user_details)
       # print('atm_dict : ', atm_dict)
       print("\n {0:*^50s}  \n ".format(" Successfully Updated Bank Details "))
       

# -------------------  Create Bank list and user or administrator given choose bank   --------------


def bank_list():
    is_invalid = True
    bankList = [ '' for i in range(len(user_details))]
    index = 0 
    for keys in user_details:
        bankList[index] = keys
        index += 1 
    if(len(user_details)):
        print("\n----- BANK LIST -------\n")
        for i in range(len(bankList)):
            print(f"[{i+1}] {bankList[i]}")
        print("\n-------------------------\n")
        select_bank = int(input("\nChoose Bank Name : "))
        if(select_bank <= len(bankList)):
            bank_name = bankList[select_bank-1]
            # print('bank Name : ', bank_name)
            return bank_name
            
        else:
            print("\nPlease Enter valid input as Number \n")   
            return is_invalid


    else:
        print("\n {0:*^100s}  \n ".format(" BANK List isn't Available because Bank List deleted by Administrator. "))
        return is_invalid

# -------------------  Create ATM list and user or administrator given choose which atm   --------------


def atm_list(select_bank):
    is_invalid = True

    if(select_bank in atm_dict):
        atmList = ['' for i in range(len(atm_dict[select_bank]))]
        index = 0 
        for keys in atm_dict[select_bank]:
           atmList[index] = keys
           index += 1   
        # print("atm_list : ", atmList)    
        if(atm_dict[select_bank]):
            print("\n------ ATM LIST --------\n")
            for i in range(len(atmList)):
                print(f"[{i+1}] {atmList[i]}")
            print("\n------------------------")

            select_atm = int(input("\nChoose ATM Name : "))
            if(select_atm <= len(atmList)):
               atm_name = atmList[select_atm-1]
               return atm_name
               
            else:
                print('\nPlease Enter valid input as Number\n')
                return is_invalid
        else:
            print("\n {0:*^100s}  \n ".format(" Sorry you can't Delete. ATM List isn't Available because ATM List deleted by Administrator.  "))
            return is_invalid
                  
    else:
        print("\n {0:*^100s}  \n ".format("Sorry you can't Delete. Bank List isn't Available because BANK List deleted by Administrator.  "))
        return is_invalid

# --------------- Show Bank Details -----------------------        

def show_users_details():   
    for bankName in user_details:
        print("\n {0:*^50s}  \n ".format(' '+ bankName +' '))

        for cardNumber in  user_details[bankName]:
            print("Card Number : " + str(cardNumber), end='\t')
            for detail in user_details[bankName][cardNumber]:
                print(str(detail) + ' : '+str(user_details[bankName][cardNumber][detail]),end='\t')

            print("\n")
        print('\n') 
# ----------------- Show ATM Details -------------------------------------------           


def show_atm_details():
    for bankName in atm_dict:
        print("\n {0:*^50s}  \n ".format(' '+ bankName +' '))
        for atmName in atm_dict[bankName]:
            print("ATM LIST : " + str(atmName), end='\t')

            for atm_detail in atm_dict[bankName][atmName]:
                print(str(atm_detail) + ' : '+str(atm_dict[bankName][atmName][atm_detail]),end='\t')
            print()
        print()    


# # -------------------- is Digit or String ----------------------- 

# def is_digit(number):
#     if(number.isdigit()):
#         return True, int(number)
#     return False, 'Please Enter Integer not String'    


# -------------- One Day limt of Transaction --------------------------------            
day_limit_transaction = 30000
# -------------- One time limt of Transaction --------------------------------            
one_time_transaction = 10000
# --------------------- How many Amout withdraw from our Bank  --------------------
total_withdraw = 0 


isContinue = True
while(isContinue):

    print("\n[1] Users \t[2] Administrator \t[3] Quit")

    users = int(input("\nChoose Option : "))
    #----------------- Normal user ---------------------------------------
    if(users == 1):

        print("\n {0:*^50s}  \n ".format(" Welcome to BOB AtM  "))
        print('If you will user other bank AtM card then 5 % cut from Your Amount ')
        condition = True


        while condition:


            is_invalid = True
            bank_name = bank_list()
            if(bank_name != is_invalid):
               atm_name = atm_list(bank_name)
               if(atm_name != is_invalid):
                   condition = userValidation(bank_name,atm_name)
               else:
                    condition = False
            else:
                condition = False

    # ----------------------------- For Administrator ---------------------------------
    elif(users==2):
    # --------------------- Username and password ---------------------------------
        print("UserName : krishna, password: krishna123 ")
        username = input("Enter Username :  ")
        password = input("Enter Password :  ")
        # print("Administrator : ",administrator_details)
        #------------------ Administrator Validation ------------------------------------
        if(username in administrator_details):
            isValidPassword = administrator_details[username]['password']
            if(isValidPassword == password):
                is_admin_continue = True
                while(is_admin_continue):
                    is_admin_continue = moreFeature()
                   
            else:
                print("\n {0:*^50s}  \n ".format(" Invalid Password "))

            
        else:
            print("\n {0:*^50s}  \n ".format(" Invalid Username "))
                     
    elif(users==3):
        isContinue = False     
        
    else:
        print("\n {0:*^50s}  \n ".format(" Wrong choice "))
                           
            

