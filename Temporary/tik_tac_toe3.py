import random
def scoreBoard(player1_name, player2_name):
	global player1_score, player2_score
	print("{0:-^50s}".format(" Score Board "))
	print(f"{player1_name} Score : {player1_score}\n{player2_name} Score : {player2_score} ")
	print("{0:-^50s}".format(''))



def isWin(player1_name, player2_name):
    global player1_score, player2_score

    # ----------------- Player 1 checking conditions  --------------
	# check horizontal 
    for i in range(0,9,3):
        if(gameBox[i] == 'X' and gameBox[i+1] == 'X' and gameBox[i+2] == 'X'):
            print(f"***** {player1_name}  Win ***** ")
            player1_score += 1 
            return 1 
     # check Vertical 
    for i in range(0, 3):
        if(gameBox[i] == 'X' and gameBox[i+3] == 'X' and gameBox[i+6] == 'X'):
          print(f"***** {player1_name}  Win ***** ")
          player1_score += 1  	
          return 1 
	
    # check diagonal 
    if(gameBox[0] == 'X' and gameBox[4] == 'X' and gameBox[8] == 'X'):
      player1_score += 1
      print(f"***** {player1_name}  win ***** ")
      return 1 
    if(gameBox[2] == 'X' and gameBox[4] == 'X' and gameBox[6] == 'X'):
      player1_score += 1
      print(f"***** {player1_name}  Win ***** ")
      return 1

	# ----------------- Player 2 checking conditions  --------------
    # check horizontal 
    for i in range(0,9,3):
        if(gameBox[i] == 'O' and gameBox[i+1] == 'O' and gameBox[i+2] == 'O'):
          player2_score += 1
          print(f"***** {player2_name}  Win ***** ")
          return 1 
     # check Vertical 
    for i in range(0, 3):
        if(gameBox[i] == 'O' and gameBox[i+3] == 'O' and gameBox[i+6] == 'O'):
          player2_score += 1
          print(f"***** {player2_name}  Win ***** ")
          return 1 
    # check diagonal 

    if(gameBox[0] == 'O' and gameBox[4] == 'O' and gameBox[8] == 'O'):
      print(f"{player2_name}  Win !!")
      player2_score += 1
      return 1 
    if(gameBox[2] == 'O' and gameBox[4] == 'O' and gameBox[6] == 'O'):
      player2_score += 1	
      print(f"***** {player2_name}  Win ***** ")
      return 1
    return  0 

def showGameBox():
    k = 3 
    print("\n\t{0:=^40s} \n".format(' Show Place '))
    for i in range(len(gameBox)):
        if(i<k):
            print('\t  '+str(i+1)+ ' ' +'|', end=" ")
            if(i==k-1):
                k += 3 
                print('\n\t{0:-^22s}'.format(''))
    # print('--->', end= ' ')
                

    
    # for i in range(len(gameBox)):
    #     if(i<k):
    #         print('\t  '+gameBox[i]+ ' ' +'|', end=" ")
    #         if(i==k-1):
    #             k += 3 
    #             print('\n\t{0:-^22s}'.format(''))

    print("\n\t{0:=^40s}\n".format(''))

def updateGameBox(player_name):
        k = 3 
        print("\n\t{0:=^40s} \n".format(' '+player_name+' Entered '))
        for i in range(len(gameBox)):
            if(i<k):
                print('\t   '+gameBox[i]+ ' ' +'|', end=" ")
                if(i==k-1):
                    k += 3 
                    print('\n\t{0:-^22s}'.format(''))
        print("\n\t{0:=^40s}\n".format(''))


def playWithFriend(player1_name, player2_name):

    # player1 = input("Enter Player 1 Name : ")
    # player2 = input("Enter Player 2 Name : ")
# 	attempt = True
    showGameBox()
   

    total_number_of_time = 0
    end_of_game = 0 
    player = 1 
    while(True):
        end_of_game = isWin(player1_name, player2_name)
        if(total_number_of_time == 9  or end_of_game == 1):
            if(total_number_of_time == 9 and end_of_game == 0):
                print(" **** Game is draw ! ****** ")
            break 
        while(True):
            if(player==1):
                player_1 = int(input(f'{player1_name} Enter Your Place  : '))
                if(player_1>0 and player_1<=9 and gameBox[player_1-1] == ' '):
                   gameBox[player_1-1] = 'X'
                   updateGameBox(player1_name)
                   player = 2 
                   break 


                else:
                	print("\nInvalid Input Please try again .... ")
                	continue 
            else:

                player_2 = int(input(f'{player2_name} Enter Your Place  : '))
                if(player_2>0 and player_2 <=9 and gameBox[player_2-1] == ' '):
                    gameBox[player_2-1] = 'O'
                    updateGameBox(player2_name)
                    player = 1 
                    break
                else:
                    print("\nInvalid Input Please try again .... ")
                    continue 
        total_number_of_time += 1 

def computerCheck():
    # for i in range(len(gameBox)):
    #     print(f'{i} : {gameBox[i]}')
    

    global getIndexPlayer 
    print('userget ', getIndexPlayer)
    if(getIndexPlayer == 0):
        if(gameBox[1] == ' '):
            gameBox[1] = 'X'
            return 1 
        elif(gameBox[3] == ' '):
             gameBox[3] = 'X'
             return 1	

             		
    if(getIndexPlayer == 1):
        if(gameBox[0] == ' '):
            gameBox[0] = 'X'
            return 1
        elif(gameBox[2] == ' '):
             gameBox[2] = 'X'	
             return 1		         



             		
    if(getIndexPlayer == 2):
        if(gameBox[1] == ' '):
            gameBox[1] = 'X'
            return 1		         
        	

        elif(gameBox[5] == ' '):
             gameBox[5] = 'X'
             return 1

    if(getIndexPlayer == 3):
        if(gameBox[0] == ' '):
            gameBox[0] = 'X'
            return 1
        elif(gameBox[6] == ' '):
             gameBox[6] = 'X'    
             return 1     			         

    if(getIndexPlayer == 5):
        if(gameBox[2] == ' '):
            gameBox[2] = 'X'
            return 1
        elif(gameBox[8] == ' '):
             gameBox[8] = 'X'
             return 1

    if(getIndexPlayer == 6):
        if(gameBox[3] == ' '):
            gameBox[3] = 'X'
            return 1
        elif(gameBox[7] == ' '):
             gameBox[7] = 'X'
             return 1

    if(getIndexPlayer == 7):
        if(gameBox[6] == ' '):
            gameBox[6] = 'X'
            return 1
        elif(gameBox[8] == ' '):
             gameBox[8] = 'X'   
             return 1


    if(getIndexPlayer == 8):
        if(gameBox[7] == ' '):
            gameBox[7] = 'X'
            return 1
        elif(gameBox[5] == ' '):
             gameBox[5] = 'X'   
             return 1     


    emptyEdge  = [ index for index in range(len(gameBox)) if(index%2 != 0 and gameBox[index] == ' ') ]
    if(emptyEdge):
        gameBox[emptyEdge[0]] = 'X'
        return 1 
    empty = [ index for index in range(len(gameBox)) if(gameBox[index] == ' ')]
    if(empty):
    	gameBox[empty[0]] = 'X'
    	return 1 
    return 0               
    

   #  row1, row2, row3, col1, col2, col3, digonalLeft, digonalRight = 0, 0, 0, 0, 0, 0, 0, 0
   #  for i in range(3):
   #      if(gameBox[i] == 'O'):
   #          row1 += 1 
   #  if(row1 == 2):
   #     for i in range(3):
   #       if(gameBox[i] == ' '):
   #          gameBox[i] = 'X' 
   #          # print('row1', i+1)
   #          return 1 
   #  for i in range(3, 6):
   #      if(gameBox[i] == 'O'):
   #          row2 += 1 
   #  if(row2 == 2):
   #     for i in range(3, 6):
   #       if(gameBox[i] == ' '):
   #          gameBox[i] = 'X' 
   #          # print('row2', i+1) 
   #          return 1       
   #  for i in range(6, 9):
   #      if(gameBox[i] == 'O'):
   #          row3 += 1 
   #  if(row3 == 2):
   #     for i in range(6, 9):
   #       if(gameBox[i] == ' '):
   #          gameBox[i] = 'X'
   #          # print('row3', i+1)  
   #          return 1      
   #  # ---------------Col------------------


   #  for i in range(0, 9, 3):
   #      if(gameBox[i] == 'O'):
   #          col1 += 1 
   #  if(col1 == 2):
   #     for i in range(0, 9, 3):
   #       if(gameBox[i] == ' '):
   #          gameBox[i] = 'X' 
   #          # print('col1', i+1)
   #          return 1 
   #  for i in range(1, 9, 3):
   #      if(gameBox[i] == 'O'):
   #          col2 += 1 
   #  if(col2 == 2):
   #     for i in range(1, 9, 3):
   #       if(gameBox[i] == ' '):
   #          gameBox[i] = 'X'
   #          # print('col2', i+1)  
   #          return 1       
   #  for i in range(2, 9, 3):
   #      if(gameBox[i] == 'O'):
   #          col3 += 1 
   #  if(col3 == 2):
   #     for i in range(2, 9, 3):
   #       if(gameBox[i] == ' '):
   #          gameBox[i] = 'X'  
   #          # print('col3', i+1)
   #          return 1  

   # # ------------Digonal -------------------------------- 

   #  for i in range(0, 9, 4):
   #      if(gameBox[i] == 'O'):
   #          digonalRight += 1 
   #  if(digonalRight == 2):
   #     for i in range(0, 9, 4):
   #       if(gameBox[i] == ' '):
   #          gameBox[i] = 'X' 
   #          # print('digonalRight', i+1)
   #          return 1 

   #  for i in range(2, 9, 2):
   #      if(gameBox[i] == 'O'):
   #          digonalLeft += 1 
   #  if(digonalLeft == 2):
   #     for i in range(0, 9, 4):
   #       if(gameBox[i] == ' '):
   #          gameBox[i] = 'X' 
   #          # print('digonalLeft', i+1)
   #          return 1 
    # return 0                 
               
                             

def playWithComputer(player1_name, player2_name):
    global getIndexPlayer
    getIndexPlayer = 0 
    showGameBox()
    total_number_of_time = 1
    end_of_game = 0 
    gameBox[4] = 'X'
    player = 2
    updateGameBox(player1_name)
    isCont, isContinue = True, True
    firstComputerInput  = True
    while(isCont):
        end_of_game = isWin(player1_name, player2_name)
        if(total_number_of_time == 9  or end_of_game == 1):
            if(total_number_of_time == 9 and end_of_game == 0):
                print(" **** Game is draw  Try Again !! ****** ")
            break 
        while(isContinue):
        	# if(total_number_of_time != 9):
                print("total_number_of_time : ", total_number_of_time)
                if(player==1):
                    # gameBox[4] = 'X'
                    if(computerCheck()):
	                    # print(f'{player1_name}  Choose Place  : {player_1}')
                        print(f'{player1_name}  Choose Place  : ')
                        total_number_of_time += 1 

                        updateGameBox(player1_name)
                        if(total_number_of_time == 9):
                            isContinue = False
                            break 

                        if(isWin(player1_name, player2_name)):
                            updateGameBox(player1_name)
                            scoreBoard(player1_name, player2_name)
                            # isCont = False


	                        # isCont = False
                            break 
                        else: 
         

	                            # scoreBoard(player1_name, player2_name)
	                            player = 2 


	                # if(firstComputerInput):
	                #     player_1 = 5 
	                #     firstComputerInput = False
	                # else:
	                #     player_1 = random.randint(1,10)
	                # if(player>0 and player_1<=9 and gameBox[player_1-1] == ' '):
	                #    print(f'{player1_name}  Choose Place  : {player_1}')
	                #    gameBox[player_1-1] = 'X'
	                #    # print('Append X', player_1)
	                #    # updateGameBox(player1_name)
	                #    end_of_game = isWin(player1_name, player2_name)
	                #    if(end_of_game):
	                #       updateGameBox(player1_name)
	                #       isCont = False
	                #       break 
	                #    else:
	                #        gameBox[player_1-1] = ' '
	                #        # print('remove X', player_1, gameBox[player_1-1])
	                #        if(computerCheck() == 0 ):
	                #           gameBox[player_1-1] = 'X'
	                #           updateGameBox(player1_name) 
	                #           end_of_game = isWin(player1_name, player2_name)
	                #           if(end_of_game):
	                #             # print("After append value : ")
	                #             updateGameBox(player1_name)  
	                #             isCont = False
	                #             break 
	                #        else:
	                #            # print("After append value : ")
	                #            updateGameBox(player1_name) 
	                #        player = 2 
	                #        break 


	                # else:
	                # 	# print("\n Invalid Input Please try again .... ")
	                # 	continue 
                else:

                    player_2 = int(input(f'{player2_name} Enter Your Place  : '))
                    if(player_2>0 and player_2 <=9 and gameBox[player_2-1] == ' '):
                        gameBox[player_2-1] = 'O'
                        getIndexPlayer =  player_2 - 1 
                        updateGameBox(player2_name)
                        total_number_of_time += 1 

                        player = 1 
                        break
                    else:
                        print("\nInvalid Input Please try again .... \n")
                        continue 
	                   
        # total_number_of_time += 1 


# gameBox =  [  [ ''  for col in range(3)]for raw in range(3)]
gameBox = [ ' ' for i in range(9)]
getIndexPlayer = 0 
isContinueGame = True
while(isContinueGame):
   player1_score, player2_score  = 0, 0 
   print("\n[1] Play With Computer \t[2] Play With Friend \t[3] Quit\n")
   user_choice = int(input("Enter Your Choice : "))
   if(user_choice == 1):
        isContinueFriend = True
        while(isContinueFriend): 
           player1, player2 = 'Computer',  input("Enter Your Name : ") 
           gameBox = [ ' ' for i in range(9)]
           scoreBoard(player1, player2)
           playWithComputer(player1, player2)
           scoreBoard(player1, player2)
           n= int(input("[1] Continue....\t[2] Exit\nYour Choice : "))
           if(n == 1):
              isContinueFriend = True
           elif(n == 2):
               isContinueFriend = False
           else:
               print("\nWrong Input ")
   elif(user_choice == 2):
        isContinueFriend = True
        while(isContinueFriend): 

           player1 = input("Enter Player 1 Name : ")
           player2 = input("Enter Player 2 Name : ")
           gameBox = [ ' ' for i in range(9)]
           scoreBoard(player1, player2)
           playWithFriend(player1, player2)
           scoreBoard(player1, player2)
           n= int(input("[1] Continue....\t[2] Exit\nYour Choice : "))
           if(n == 1):
              isContinueFriend = True
           elif(n == 2):
               isContinueFriend = False
           else:
               print("\nWrong Input ")
   elif(user_choice ==3):
        isContinueGame= False     
        print("Thank you !! ")
   else:
   	  print("Invalid Input ")
