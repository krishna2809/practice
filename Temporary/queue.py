class Queue:
    def __init__(self, queueSize, arr, front,rear):
        self.arr = arr
        self.size = queueSize
        self.front = front
        self.rear = rear

    def enqueue(self, element):
        if(self.rear == self.size -1 ):
            print("******** Queue is Overflow ********\n")
           
        else:
            if(self.front == -1):
                self.front  = 0 
            self.arr[self.rear] = element

    def dequeue(self):
        if( self.rear == -1):
            print("******** Queue is underflow ********\nPlease First Enter a element \n")

            
        else:

            removeElement = self.arr[self.front]
            
            print(f"{removeElement} has been removed :\n")
            for i in range(0, self.rear):
                self.arr[i]  = self.arr[i+1]
            if(self.rear <  self.size):
                self.arr[self.rear] =  0
            self.rear -= 1 

          
    def peek(self):
        if(self.front <= self.rear ):
            print(f"Front element of Queue  : {self.arr[self.front]} ") 
        else:
            print("******** Queue is Empty ******** \n")   
   

queueSize = int(input("Enter size of queue : "))

arr = list(map(int, input("Enter element With space : ").split(" ")))
queue = Queue(queueSize, arr, 0, queueSize-1)

while True:
    choice   = int(input("Enter you choice Number:\n[1] Enqueue \n[2] Dequeue \n[3] peek \n[4] Exit \n" ))

    if(choice == 1):
        queue.enqueue(input("Enter a element : "))

    if(choice == 2):
        queue.dequeue()
    if(choice == 3):
        queue.peek()   
    if(choice == 4):
        del queue
        print("Thank you for doing some operations \n")
        break         


