print("\n{:*^60s}".format(" Welcome to Determinnet Matrix Program "))


def determinant_matrix(lst):
    
    main_ans = (lst[0][0]* ((lst[1][1]*lst[2][2]) - (lst[1][2]*lst[2][1])))  - (lst[0][1]*( (lst[1][0]*lst[2][2]) - (lst[1][2] * lst[2][0])))   + (lst[0][2]*((lst[1][0]*lst[2][1]) - (lst[1][1] * lst[2][0])) ) 
    return main_ans



lst  = [  [  0 for j in range(3)]  for i in range(3) ]


for i in range(3):
        # column = []
        column =  [i  for i in range(3)]
        for j in range(3):
             value = int(input(f"\nEnter value of [{i+1}][{j+1}] : "))
             column[j] = value
        lst[i] = column

print("\n***** Matrix  *****\n ")
for i in lst:
    for value in  i:
        print(value , end=" ")
    print("")



ans = determinant_matrix(lst)
print("\nResult : ",  ans)
