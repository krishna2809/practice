def lcm(firstValue, secondValue):
    multi = 1 
    if(firstValue > secondValue):
       maxValue = firstValue 
    else:
       maxValue = secondValue
    for i in range(2, maxValue):
        if(firstValue ==1 and secondValue ==1 ):
            break 
        while(firstValue%i == 0 or secondValue % i == 0):
            multi *= i 
            if(firstValue % i == 0 ):
              firstValue = firstValue//i 
            if(secondValue % i == 0 ):
               secondValue = secondValue//i 
    return multi         	





numberOfElement =  int(input("\nEnter number of Element : "))

numberOfElementList = [ int(input(f"Enter {i+1} value :  ")) for i in range(numberOfElement)]


firstValue = numberOfElementList[0]    

for i in range(1, numberOfElement):
    # print("value of lst ", numberOfElementList[i])
    ansOfLcm = lcm(firstValue , numberOfElementList[i])
    firstValue = ansOfLcm


print(firstValue)    