# administator ----> UserName : krishna, password: krishna123

import random

limit = 0
#bank_name = ' '
#atm_name = ' '

user_details = {
    "BOB" : {
        1000 : {"username" : "krishna", "userpin":22,"balance": 5000,"mobile number":9714986076,"Account Number":1000},
        2000 : {"username" : "vishal", "userpin":33,"balance": 2000,"mobile number":9563207412,"Account Number":2000},
    },
    "SBI" : {
        3000 : {"username" : "krishna", "userpin":44,"balance": 2000,"mobile number":9756300586,"Account Number":1100},
        4000 : {"username" : "vishal", "userpin":55,"balance": 3000,"mobile number":9874562310,"Account Number":1200}
    }
}

atm_dict = {
    "BOB" : {
        "bob_atm_one" : {"total_atm_amount" : 5000,"Location" : "rajkot"},
        "bob_atm_two" : {"total_atm_amount" : 60000,"Location" : "Ahmedabad"}
    },
    "SBI" : {
        "sbi_atm_one" : {"total_atm_amount" : 40000,"Location" : "rajkot"},
        "sbi_atm_two" : {"total_atm_amount" : 60000,"Location" : "Ahmedabad"}
    }
}
administrator_details = {
    'krishna': {'password': 'krishna123', 'email_id': 'krishna_singh@gamil.com', 'Phone Number': 9999999999}
}


def userValidation(bankname,atmname):
    # global user_details
    # if bankname in bankList:
    #     if atmname in atmList:
    #         getDetails(bankname)
    # else:
    #     print("\nInvalid bank name\n")
    #     x = input("\nDo you want to continue yes or no :  ")
    #     if(x == "yes" or x == "YES"):
    #         return True
    #     else:
    #         return False
    return getDetails(bankname)






def getDetails(bankname):
    is_invalid = True
    user_detail = user_details[bankname]
    card_number = int(input("\nEnter Card Number : "))
    pin_number =  int(input("\nEnter PIN Number  : "))
    data = user_detail.get(card_number,"Invalid Card Number")
    if card_number in user_detail:
        if pin_number in data.values():
            displayOption(data)
            is_invalid = False
        else: 
            print("\n {0:*^50s}  \n ".format(" Invalid Pin Number "))
            return is_invalid
    else:
        print("\n {0:*^50s}  \n ".format(" Invalid Card Number "))
        return is_invalid          


          

def displayDetails(detail):
    print("\n {0:*^60s}\n".format(" YOUR DETAILS "))

    print(f"Bank Name : {detail['username']}  \t\tAccount Number : {detail['Account Number']}\n")
    print(f"Balance   : {detail['balance']}    \t\tMobile Number : {detail['mobile number']}\n")
    print("\n {0:*^60s}".format(""))

def displayOption(data):
    print("\n[1] Withdraw \t[2] Deposite \t[3] Display \n")
    choice = int(input("\nChoose option : "))
    if(choice == 1):
        withdraw(data)
        displayOption(data)
    elif(choice == 2):
        deposite(data)
        displayOption(data)
    elif(choice == 3):
        displayDetails(data)
        displayOption(data)
    # elif(choice == 4):
    #     moreFeature(data)
    else:
        print("\nWrong choice \n")


def withdraw(data):
    global limit,atm_dict
    withdraw_amount = int(input("\nEnter Withdraw Amount : "))
    atm = atm_dict[bank_name][atm_name]
    user_amount = data['balance']
    atm_amount = atm['total_atm_amount']
    if(limit < 5):
        if(withdraw_amount%100 == 0 ):
            if(withdraw_amount <= atm_amount):
                if(withdraw_amount <= user_amount):
                    if bank_name == "BOB":
                        total_amount = user_amount - withdraw_amount
                        atm_amount = atm_amount - withdraw_amount
                        data['balance'] = total_amount
                        atm['total_atm_amount'] = atm_amount
                        #print(data['balance'])
                        # print(atm['total_atm_amount'])
                        limit += 1 
                        print("\n {0:*^50s}  \n ".format(" Successfully Withdraw Amount "))
                        print("\n {0:*^50s}  \n ".format(' Your Available Amount : ' + str(data['balance'])+' '))

                    else:
                        updated_withdraw_amount = withdraw_amount*0.05
                        after_cut_five_per =  user_amount -  updated_withdraw_amount
                        if(withdraw_amount <= after_cut_five_per):
                            total_amount = user_amount - withdraw_amount - updated_withdraw_amount
                            atm_amount = atm_amount - withdraw_amount - updated_withdraw_amount
                            data['balance'] = total_amount
                            atm['total_atm_amount'] = atm_amount
                            # print(data['balance'])
                            # print(atm['total_atm_amount'])
                            limit += 1
                            print("\n {0:*^50s}  \n ".format(" Successfully Withdraw Amount "))
                            print("\n {0:*^50s}  \n ".format(' Your Available Amount : ' + str(data['balance'])+' '))
                        else:
                            print("\n {0:*^100s}  \n ".format(" You can't withdraw Amount because " + str(after_cut_five_per) + ' After cut 5 % Amount '))
              
                           

                else:
                    print(f"\nEnter withdraw amount less than or equal to {user_amount}")
            else:
                print(f"\nAtm Available Amount :  {atm_amount}")
        else:
            print("\n {0:*^50s} \n".format(" Multiple of 100  "))       
    else:
        print(f"\nYour one day limit is over.\n")

def deposite(data):
    global atm_dict, atm_name, bank_name
    print('data ', data )
    print('bank_name', bank_name, '\natm_dict ', atm_name)
    deposite_amount = int(input("\nEnter Deposite Amount : "))
    if(deposite_amount%100 == 0 ):
        atm = atm_dict[bank_name][atm_name]
        user_amount = data['balance']
        atm_amount = atm['total_atm_amount']
        update_amount = user_amount + deposite_amount
        data['balance'] = update_amount
        atm_amount = atm_amount + deposite_amount
        atm['total_atm_amount'] = atm_amount
        # print(data['balance'])
        # print(atm['total_atm_amount']) 
        print("\n {0:*^50s}  \n ".format(" Successfully Deposit Amount "))
        print("\n {0:*^50s}  \n ".format(' Your Available Amount : ' + str(data['balance'])+' '))
    else:
        print("\n {0:*^50s} \n".format(" Multiple of 100  "))     
# def moreFeature(data):
#     print("\n[1] User \t[2] ATM \t[3] Banks \t[4] Add Money in ATM")
#     choice = int(input("\nChoose option : "))
#     if(choice == 1):
#         print("\n[1] Insert User \t[2] Delete User \t[3] Update User")
#         user_choice = int(input("\nChoose option : "))
#         if(user_choice == 1):
#             inserUser(user_details)
#         elif(user_choice == 2):
#             deleteUser(user_details)
#         elif(user_choice == 3):
#             updateUser(user_details)
#         else:
#             print('\nWrong choice\n')
#     elif(choice == 2):
#         deposite(data)
#         displayOption(data)
#     elif(choice == 3):
#         displayDetails(data)
#         displayOption(data)
#     elif(choice == 4):
#         moreFeature(data)
#     else:
#         print("\nWrong choice\n")

def moreFeature():
    print("\n[1] User \t[2] ATM \t[3] Banks \t[4] Add Money in ATM")
    choice = int(input("\nChoose option : "))
    if(choice == 1):
        print("\n[1] Insert User \t[2] Delete User \t[3] Update User")
        user_choice = int(input("\nChoose option : "))
        if(user_choice == 1):
            insertUser()
        elif(user_choice == 2):
            deleteUser()
        elif(user_choice == 3):
            updateUser()
        else:
            print('\nWrong choice\n')
    elif(choice == 2):
        # deposite(data)
        # displayOption(data)
        print("\n[1] Insert ATM \t[2] Delete ATM \t[3] Update ATM ")
        user_choice = int(input("\nChoose option : "))
        if(user_choice == 1):
            insertAtm()
        elif(user_choice == 2):
            deleteAtm()
        elif(user_choice == 3):
            updateAtm()
        else:
            print('\nWrong choice\n')
        
    elif(choice == 3):
        print("\n[1] Insert Bank \t[2] Delete Bank \t[3] Update Bank ")
        user_choice = int(input("\nChoose option : "))
        if(user_choice == 1):
            insertBank()
        elif(user_choice == 2):
            deleteBank()
        elif(user_choice == 3):
            updateBank()
            
        else:
            print('\nWrong choice\n')
    elif(choice == 4):
        insert_money_atm()
    else:
        print("\nWrong choice\n")


def insertUser():
    bank_name = bank_list()
    is_invalid = True
    if(bank_name != is_invalid):
        # global user_details, bank_name
        # print(userDetails)
        print('user_details', user_details)
        # print('user_details[bank_name][user_card_number]', user_details[bank_name][user_card_number])
        user_dict = {}
        user_name = input("\nEnter User Name : ")
        user_pin = random.randint(10,99)
        user_card_number = random.randint(1000,9999)
        balance = int(input("\nEnter balance : "))
        # bank_name = input("\nEnter Bank Name : ")
        mobile_number = int(input("\nEnter Mobile Number : "))
        Account_number = int(input("\nEnter Account Number : "))
        user_dict = {"username":user_name,"userpin":user_pin,"balance": balance,"bankname":bank_name,"mobile number":mobile_number,"Account Number":Account_number}
        user_details[bank_name][user_card_number] = user_dict
        print("\n {0:*^50s}  \n ".format(" Successfully User Details Inserted "))


        # print('updated details 1', user_details)
        # print('updated details 2', userDetails)
        # print('updated user_details[bank_name][user_card_number]', user_details[bank_name][user_card_number])





def deleteUser():
    is_invalid  = True
    bank_name = bank_list()
    if(bank_name != is_invalid): 

        # global user_details,bank_name
        # print("User Deatisl---->  ",user_details[bank_name])
        card_number = int(input("\nEnter card Number : "))
        if(card_number in user_details[bank_name]):

            del user_details[bank_name][card_number]
            print("\n {0:*^50s}  \n ".format(" Successfully User Details Deleted "))
        else:  
            print("\n {0:*^50s}  \n ".format(" Invalid Card Number "))

        print(user_details)
    
def updateUser():
    is_invalid = True
    bank_name = bank_list()
    if(bank_name != is_invalid):
    # global user_details,bank_name
        user_dict = {}
        user_card_number = int(input("\nEnter Card Number : "))
        #print(userDetails[bank_name][user_card_number])
        if user_card_number in user_details[bank_name]:
            user_name = input("\nEnter User Name : ")
            user_pin = input("\nEnter pin number ")
            mobile_number = int(input("\nEnter Mobile Number : "))
            user_details[bank_name][user_card_number]['username'] = user_name
            user_details[bank_name][user_card_number]['userpin'] = user_pin
            user_details[bank_name][user_card_number]['mobile number'] = mobile_number
            print("\n {0:*^50s}  \n ".format(" Successfully Update User Details "))

            print(user_details)
        else:
            print("\n {0:*^50s}  \n ".format(" Invalid Card Number "))

def insertAtm():
    is_invalid = True
    bank_name = bank_list()

    if(bank_name != is_invalid):
        which_atm = input("\nEnter ATM Name : ")
        location = input("\nEnter Location of ATM : ")
        atm_amount = int(input("\nEnter ATM Amount : "))
        # {"total_atm_amount" : 40000,"Location" : "rajkot"},
        atm_details = {"total_atm_amount": atm_amount, 'Location': location}
        atm_dict[bank_name][which_atm] = atm_details
        print("\n {0:*^50s}  \n ".format(" Successfully Insert ATM Details "))
        print("atm_dict ---> ", atm_dict)


def deleteAtm():
    is_invalid = True
    bank_name = bank_list()
    if(bank_name != is_invalid):
        atm_name =  atm_list(bank_name)
        if(atm_name != is_invalid):
        # index = 0 
        # for keys in atm_dict[bank_name]:
        #    atmList[index] = keys
        #    index += 1    

        # print("\n------ ATM LIST --------\n")
        # for i in range(len(atmList)):
        #     print(f"[{i+1}] {atmList[i]}")
        # print("\n------------------------")

        # select_atm = int(input("\nChoose ATM Name : "))
        # if(select_atm <= len(atmList)):
        #    atm_name = atmList[select_atm-1]
        #    del atm_dict[bank_name][atm_name]
        #    print("\n {0:*^50s}  \n ".format(" Successfully Deleted ATM Details "))
           # print(atm_dict[bank_name])
           # atm_name = atmList[select_atm-1]
           del atm_dict[bank_name][atm_name]
           print(atm_dict)
           print("\n {0:*^50s}  \n ".format(" Successfully Deleted ATM Details "))   
        
def updateAtm():
    is_invalid = True
    bank_name = bank_list()
    if(bank_name != is_invalid):
        atm_name = atm_list(bank_name)
        if(atm_name != is_invalid):
        # if(bank_name != is_invalid and atm_name != is_invalid):
            atm_details = atm_dict[bank_name][atm_name]
            atm_details['Location'] = input("\nEnter ATM location : ")
            atm_details['total_atm_amount'] = int(input("\nEnter Amount : "))
            print(atm_dict[bank_name])

            print("\n {0:*^50s}  \n ".format(" Successfully Updated  ATM Details "))

def insert_money_atm():
    is_invalid = True
    bank_name = bank_list()
    if(bank_name != is_invalid):
        atm_name = atm_list(bank_name)
        if(atm_name != is_invalid):
    # if(bank_name != is_invalid and atm_name != is_invalid):
            print(atm_dict[bank_name][atm_name])
            atm_dict[bank_name][atm_name]['total_atm_amount'] += int(input("\nEnter Amount for insert Money into ATM :  "))
            print('Updated amount : ',atm_dict[bank_name][atm_name])
            print("\n {0:*^50s}  \n ".format(" Successfully Insert Money in  ATM "))


def insertBank():
    new_Bank_name = input("\nEnter new Bank Name : ")
    user_details[new_Bank_name] = {}
    atm_dict[new_Bank_name] = {}
    print("\n {0:*^50s}  \n ".format(" Successfully Insert Bank Details "))
    print(user_details)

def deleteBank():
    is_invalid = True
    bank_name = bank_list()
    if(bank_name != is_invalid):
       del user_details[bank_name]
       if(bank_name in atm_dict):
          del atm_dict[bank_name]
          print('user_details : ', user_details)
          print('atm_dict : ', atm_dict)
          print("\n {0:*^50s}  \n ".format(" Successfully Delete Bank Details "))


    # bankList = [ '' for i in range(len(user_details))]
    # index = 0 
    # for keys in user_details:
    #     bankList[index] = keys
    #     index += 1 
    # print("\n----- BANK LIST -------\n")
    # for i in range(len(bankList)):
    #     print(f"[{i+1}] {bankList[i]}")
    # print("\n-------------------------\n")
    # delete_bank = int(input("\nChoose Bank for Delete : "))
    # if(delete_bank <= len(bankList)):
       # del user_details[bankList[delete_bank-1]]
       # del atm_dict[bankList[delete_bank-1]]
    #    # print('user_details : ', user_details)
    #    # print('atm_dict : ', atm_dict)
    #    print("\n {0:*^50s}  \n ".format(" Successfully Delete Bank Details "))
    # else:
    #     print("\nPlease Enter valid input as Number \n")
           


def updateBank():
    # bankList = [ '' for i in range(len(user_details))]
    # index = 0 
    # for keys in user_details:
    #     bankList[index] = keys
    #     index += 1 
    # print("\n----- BANK LIST -------\n")
    # for i in range(len(bankList)):
    #     print(f"[{i+1}] {bankList[i]}")
    # print("\n-------------------------\n")
    # update_bank = int(input("\nChoose Bank for Update  : "))
    # if(update_bank <= len(bankList)):
    #    bank_details =  user_details[bankList[update_bank-1]]
    #    atm_details =  atm_dict[bankList[update_bank-1]]
    #    update_bank_name = input("Enter Update Bank Name : ")
    #    user_details[update_bank_name] = bank_details 
    #    atm_dict[update_bank_name] = atm_details
    #    del user_details[bankList[update_bank-1]]
    #    del atm_dict[bankList[update_bank-1]]
    #    # print('user_details : ', user_details)
    #    # print('atm_dict : ', atm_dict)
    #    print("\n {0:*^50s}  \n ".format(" Successfully Updated Bank Details "))
    # else:
    #     print("\nPlease Enter valid input as Number \n")
    is_invalid = True
    bank_name = bank_list()
    if(bank_name != is_invalid):
       bank_details =  user_details[bank_name]
       atm_details =  atm_dict[bank_name]
       update_bank_name = input("Enter Update Bank Name : ")
       user_details[update_bank_name] = bank_details 
       atm_dict[update_bank_name] = atm_details
       del user_details[bank_name]
       del atm_dict[bank_name]
       print('user_details : ', user_details)
       print('atm_dict : ', atm_dict)
       print("\n {0:*^50s}  \n ".format(" Successfully Updated Bank Details "))
       


def bank_list():
    is_invalid = True
    bankList = [ '' for i in range(len(user_details))]
    index = 0 
    for keys in user_details:
        bankList[index] = keys
        index += 1 
    print('bank List : ', bankList)    
    if(len(user_details)):
        print("\n----- BANK LIST -------\n")
        for i in range(len(bankList)):
            print(f"[{i+1}] {bankList[i]}")
        print("\n-------------------------\n")
        select_bank = int(input("\nChoose Bank Name : "))
        if(select_bank <= len(bankList)):
            bank_name = bankList[select_bank-1]
            # print('bank Name : ', bank_name)
            return bank_name
            
        else:
            print("\nPlease Enter valid input as Number \n")   
            return is_invalid


    else:
        print("\n {0:*^100s}  \n ".format(" BANK list isn't Available because bank list deleted by Administrator. "))
        return is_invalid



def atm_list(select_bank):
    is_invalid = True

    if(select_bank in atm_dict):
        # bank_name = bankList[select_bank-1]
        atmList = ['' for i in range(len(atm_dict[select_bank]))]
        index = 0 
        for keys in atm_dict[select_bank]:
           atmList[index] = keys
           index += 1    
    
        print("\n------ ATM LIST --------\n")
        for i in range(len(atmList)):
            print(f"[{i+1}] {atmList[i]}")
        print("\n------------------------")

        select_atm = int(input("\nChoose ATM Name : "))
        if(select_atm <= len(atmList)):
           atm_name = atmList[select_atm-1]
           return atm_name

           # condition = userValidation(bank_name,atm_name)
           
        else:
            print('\nPlease Enter valid input as Number\n')
            return is_invalid
            

        # if(is_not_bank == False and is_not_atm == False):
           # condition = userValidation(bank_name,atm_name)
           # is_not_bank = False
    else:
          print("\n {0:*^100s}  \n ".format(" ATM List isn't Available because ATM list deleted by Administrator.  "))
          return is_invalid



    



    


isContinue = True
while(isContinue):

    print("\n[1] Users \t[2] Administrator \t[3] Quit")
    bankList = [ '' for i in range(len(user_details))]
    index = 0 
    for keys in user_details:
        bankList[index] = keys
        index += 1 

    users = int(input("\nChoose Option : "))
    if(users == 1):



        print("\n {0:*^50s}  \n ".format(" Welcome to BOB AtM  "))
        print('If you will user other bank AtM card then 5 % cut from Your Amount ')
        condition = True


        while condition:


            is_invalid = True
            bank_name = bank_list()
            if(bank_name != is_invalid):
               atm_name = atm_list(bank_name)
               if(atm_name != is_invalid):
                   condition = userValidation(bank_name,atm_name)
               else:
                    condition = False
            else:
                condition = False

                    
            # bankList = user_details.keys()
             


            # print("Choose Bank Name : ")
            # print("\n-----------------------")
            # for i in range(len(bankList)):
            #     print(f"[{i+1}] {bankList[i]}")
            # print("-----------------------\n")

            # select_bank = int(input("\nChoose Bank Name : "))
            # if(is_not_bank):




            # if(len(user_details)):
            #     print("\n----- BANK LIST -------\n")
            #     for i in range(len(bankList)):
            #         print(f"[{i+1}] {bankList[i]}")
            #     print("\n-------------------------\n")

            #     select_bank = int(input("\nChoose Bank Name : "))
            #     if(select_bank <= len(bankList)):
            #         if(select_bank in atm_dict):
            #             bank_name = bankList[select_bank-1]
            #             atmList = ['' for i in range(len(atm_dict[bank_name]))]
            #             index = 0 
            #             for keys in atm_dict[bank_name]:
            #                atmList[index] = keys
            #                index += 1    
                    
            #             print("\n------ ATM LIST --------\n")
            #             for i in range(len(atmList)):
            #                 print(f"[{i+1}] {atmList[i]}")
            #             print("\n------------------------")

            #             select_atm = int(input("\nChoose ATM Name : "))
            #             if(select_atm <= len(atmList)):
            #                atm_name = atmList[select_atm-1]
            #                condition = userValidation(bank_name,atm_name)
                           
            #             else:
            #                 print('\nPlease Enter valid input as Number\n')
                            

            #             # if(is_not_bank == False and is_not_atm == False):
            #                # condition = userValidation(bank_name,atm_name)
            #                # is_not_bank = False
            #         else:
            #               print("\n {0:*^100s}  \n ".format(" ATM List isn't Available because ATM list deleted by Administrator.  "))
            #               condition = False
                               
            #     else:
            #         print("\nPlease Enter valid input as Number \n")   
                    
            # else:
            #     print("\n {0:*^100s}  \n ".format(" BANK list isn't Available because bank list deleted by Administrator.  "))
            #     condition = False
                         
            # if(is_not_atm and is_not_bank == False):        
            #     # atmList = atm_dict[bank_name].keys()
            #     atmList = ['' for i in range(len(atm_dict[bank_name]))]
            #     index = 0 
            #     for keys in atm_dict[bank_name]:
            #        atmList[index] = keys
            #        index += 1    
                
            #     print("\n------ ATM LIST --------\n")
            #     for i in range(len(atmList)):
            #         print(f"[{i+1}] {atmList[i]}")
            #     print("\n------------------------")

            #     select_atm = int(input("\nChoose ATM Name : "))
            #     if(select_atm <= len(atmList)):
            #         atm_name = atmList[select_atm-1]
            #         is_not_atm = False
            #     else:
            #         print('\nPlease Enter valid input as Number\n')
            #         is_not_atm = True    

            # if(is_not_bank == False and is_not_atm == False):
            #    condition = userValidation(bank_name,atm_name)

    elif(users==2):
        username = input("Enter Username :  ")
        password = input("Enter Password :  ")
        # print("Administrator : ",administrator_details)

        if(username in administrator_details):
            isValidPassword = administrator_details[username]['password']
            if(isValidPassword == password):

                # print("\n----- BANK LIST -------\n")
                # for i in range(len(bankList)):
                #     print(f"[{i+1}] {bankList[i]}")
                # print("\n------------------------\n")

                # select_bank = int(input("\nChoose Bank Name : "))
                # if(select_bank <= len(bankList)):
                #     bank_name = bankList[select_bank-1]
                #     atmList = ['' for i in range(len(atm_dict[bank_name]))]
                #     index = 0 
                #     for keys in atm_dict[bank_name]:
                #        atmList[index] = keys
                #        index += 1    

                #     print("\n------ ATM LIST --------\n")
                #     for i in range(len(atmList)):
                #         print(f"[{i+1}] {atmList[i]}")
                #     print("\n------------------------")

                #     select_atm = int(input("\nChoose ATM Name : "))
                #     if(select_atm <= len(atmList)):
                #         atm_name = atmList[select_atm-1]
                #         moreFeature(bank_name, atm_name)
                        moreFeature()

                        
                
                #     else:
                #         print('\nPlease Enter valid input as Number\n') 
                

                # else:
                #     print("\nPlease Enter valid input as Number \n")   
                   
            else:
                print("\n {0:*^50s}  \n ".format(" Invalid Password "))

            
        else:
            print("\n {0:*^50s}  \n ".format(" Invalid Username "))
                     
    elif(users==3):
        isContinue = False     
        
    else:
        print("\nWrong choice\n")               
            
