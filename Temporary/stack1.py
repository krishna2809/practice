
def push(arr, element):
        global top, size 
        if(top < size):
            top += 1 
            arr[top] = element
        else:
            print("\n******** Stack is Overflow ******** \n")

def pop(arr):
        global top, size 
        if(top > -1 ):
            removeElement = arr[top]
            print(f"{removeElement} has been removed :\n")
            top -= 1 
        else:
            print("******** Stack is underflow ********\n   Please First Enter a element \n")
                 
def peep(arr):
         global top, size 
         if(top > -1):
            print(f"Top element of Stack : {arr[top]} ") 
         else:
            print("******** Stack is Empty ******** \n")   
def show_stack(arr):
        global top, size 
        if(top > -1):
            for i in range(top+1):
                print(arr[i], end=" ")  
        else:
              print("******** Stack is empty ********  ")
        print("\n")         


print("\n {:*^60s} ".format(" Welcome to Stack Program "))
stackSize = int(input("\nEnter size of stack : "))
arr = [ input(f"\nEnter Your {i+1} Element : ") for i in range(stackSize)]

#stack = Stack(stackSize, arr)
size = stackSize - 1  
top =  size 
while True:
    choice   = int(input("\nEnter you choice NUmber:\n[1] Push \n[2] pop \n[3] peep \n[4] showStack \n[5] Exit \n" ))

    if(choice == 1):
        push(arr, input("\nEnter a element : "))

    elif(choice == 2):
        pop(arr)
    elif(choice == 3):
         peep(arr)   
    elif(choice == 4):
        show_stack(arr)
    elif(choice == 5):
       
        print("\nThank you for doing some operations \n")
        break   
    else:
        print("\n******* Invalid  Number *********")     

          
